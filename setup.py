# -*- coding: utf-8 -*-

import re
import sys


from setuptools import setup


version = re.search(
    '^__version__\s*=\s*"(.*)"',
    open('dbmon/dbmon.py').read(),
    re.M
    ).group(1)

INSTALL_REQUIRES = [
    'trollius',
    'argparse'
]

if sys.version_info[0:2] < (2, 7):
    INSTALL_REQUIRES.append("MySQL_python")
elif sys.version_info[0:2] < (3, 5):
    INSTALL_REQUIRES.append("pymysql<1.0.0")
else:
    INSTALL_REQUIRES.append("pymysql")

setup(
    name='dbmon',
    description='Database performance monitor',
    long_description='',
    version=version,
    packages=[
        'dbmon',
    ],
    entry_points={
        'console_scripts': [
            'dbmon = dbmon.dbmon:main'
        ]
    },
    include_package_data=True,
    install_requires=INSTALL_REQUIRES,
    test_suite='tests',
)
