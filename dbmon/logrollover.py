# -*- coding: utf-8 -*-
import logging

import re
import types
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler
import time
import gzip
import os


class CompressedTimedRotatingFileHandler(TimedRotatingFileHandler):
    def __init__(self, filename, when='h', interval=1, backupCount=0, encoding=None, delay=False, utc=False, mode='w'):
        TimedRotatingFileHandler.__init__(
            self, filename=filename, when=when, interval=interval, 
            backupCount=backupCount, encoding=encoding, delay=delay, utc=utc
        )
        if self.stream:
            os.unlink(self.stream.name)
            self.stream.close()
            self.stream = None
        self.mode = mode
        self.stream = self._open()
        self._time = None
        self._flush = time.time()
        self.when = when.upper()
        if self.when == 'S':
            self.interval = 1 # one second
            self.suffix = "%Y-%m-%d_%H-%M-%S"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}(\.\w+)?$"
        elif self.when == 'M':
            self.interval = 60 # one minute
            self.suffix = "%Y-%m-%d_%H-%M"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}(\.\w+)?$"
        elif self.when == 'H':
            self.interval = 60 * 60 # one hour
            self.suffix = "%Y-%m-%d_%H"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}(\.\w+)?$"
        elif self.when == 'D' or self.when == 'MIDNIGHT':
            self.interval = 60 * 60 * 24 # one day
            self.suffix = "%Y-%m-%d"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}(\.\w+)?$"
        elif self.when.startswith('W'):
            self.interval = 60 * 60 * 24 * 7 # one week
            if len(self.when) != 2:
                raise ValueError("You must specify a day for weekly rollover from 0 to 6 (0 is Monday): %s" % self.when)
            if self.when[1] < '0' or self.when[1] > '6':
                raise ValueError("Invalid day specified for weekly rollover: %s" % self.when)
            self.dayOfWeek = int(self.when[1])
            self.suffix = "%Y-%m-%d"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}(\.\w+)?$"
        else:
            raise ValueError("Invalid rollover interval specified: %s" % self.when)

        self.extMatch = re.compile(self.extMatch)

    def format(self, record):
        cur_time = time.time()
        str_time = str(datetime.fromtimestamp(cur_time))
        if isinstance(record, logging.LogRecord):
            return str_time + " # " + str(record.msg)
        else:
            return str_time + " # " + str(record)

    def emit(self, record):
        try:
            if self.shouldRollover(record):
                self.doRollover()
            if self.stream is None:
                self.stream = self._open()
            try:
                msg = self.format(record)
                stream = self.stream
                fs = "%s\n"
                if not hasattr(types, "UnicodeType"):  # if no unicode support...
                    stream.write(fs % msg)
                else:
                    try:
                        if (isinstance(msg, unicode) and
                                getattr(stream, 'encoding', None)):
                            fs = fs.decode(stream.encoding)
                            try:
                                stream.write(fs % msg)
                            except UnicodeEncodeError:
                                # Printing to terminals sometimes fails. For example,
                                # with an encoding of 'cp1251', the above write will
                                # work if written to a stream opened or wrapped by
                                # the codecs module, but fail when writing to a
                                # terminal even when the codepage is set to cp1251.
                                # An extra encoding step seems to be needed.
                                stream.write((fs % msg).encode(stream.encoding))
                        else:
                            stream.write(fs % msg)
                    except UnicodeError:
                        stream.write(fs % msg.encode("UTF-8"))
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                self.handleError(record)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def _open(self):
        if not hasattr(self, 'suffix') or not self.suffix:
            return gzip.open(self.baseFilename, self.mode)

        currentTime = int(time.time())
        if self.utc:
            timeTuple = time.gmtime(currentTime)
        else:
            timeTuple = time.localtime(currentTime)
        dfn = self.baseFilename + "." + time.strftime(self.suffix, timeTuple) + ".gz"

        return gzip.open(dfn, self.mode)

    def computeRollover(self, currentTime):
        currentTime = currentTime - (currentTime % self.interval)
        return TimedRotatingFileHandler.computeRollover(self, currentTime)

    def doRollover(self):
        currentTime = int(time.time())
        if self.stream:
            self.stream.close()
            self.stream = None
        if self.backupCount > 0:
            for s in self.getFilesToDelete():
                print("Removing log file: {0}".format(s))
                os.remove(s)
        self.stream = self._open()
        newRolloverAt = self.computeRollover(currentTime)
        while newRolloverAt <= currentTime:
            newRolloverAt = newRolloverAt + self.interval
        #If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dstNow = time.localtime(currentTime)[-1]
            dstAtRollover = time.localtime(newRolloverAt)[-1]
            if dstNow != dstAtRollover:
                if not dstNow:  # DST kicks in before next rollover, so we need to deduct an hour
                    newRolloverAt = newRolloverAt - 3600
                else:           # DST bows out before next rollover, so we need to add an hour
                    newRolloverAt = newRolloverAt + 3600
        self.rolloverAt = newRolloverAt
