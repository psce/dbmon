# -*- coding: utf-8 -*-
import functools

from logrollover import CompressedTimedRotatingFileHandler

__version__ = "0.1.2"

import os
import os.path
import sys
import time
import signal
import logging
import argparse
import gzip
import shlex
import json
import traceback
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler

try:
    from ConfigParser import ConfigParser
except ImportError:
    from configparser import ConfigParser

try:
    import pymysql
except ImportError:
    import MySQLdb as pymysql

import trollius as asyncio
from trollius.subprocess import PIPE


# DEFAULTS

PERIODIC_INTERVAL = 5

CONTINOUS_RETRIES = 5
CONTINOUS_RETRIES_TIMEOUT = 10

MYSQL_HOST = 'localhost'
MYSQL_USER = None
MYSQL_PASSWD = None
MYSQL_DEFAULT_FILE = None

ROTATE_SCHEDULE = 'H'
NUMBER_OF_FILES_TO_KEEP = 96


def exception_handler(x, y):
    pass


class DbMon():
    log_dir = "/var/log/dbmon"
    log_level = logging.INFO

    def __init__(self, options):
        
        self.get_config(options.config)

        if options.debug:
           self.log_level = logging.DEBUG

        try:
            self.log_dir = self.config['global']['log_dir']
        except KeyError:
            pass

        self.log_file = self.log_dir + "/dbmon.log"

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.log_level)
        handler = TimedRotatingFileHandler(
            self.log_file, when="W0", interval=1, backupCount=14)
        handler.setLevel(self.log_level)
        formatter = logging.Formatter("%(asctime)s #  %(message)s")
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def run(self):
        if not self.config:
            raise Exception("Not configured")

        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

        self.loop = asyncio.get_event_loop()
        self.loop.set_exception_handler(exception_handler)

        async_tasks = []
        tasks = []
        for item in self.config.keys():
            if item == "daemon" or item == "global":
                self.logger.debug(
                    "Global options: {0}".format(json.dumps(self.config[item])))
                continue

            self.config[item]['log_dir'] = self.log_dir
            self.config[item]['log_level'] = self.log_level

            self.logger.debug(
                "{0} options: {1}".format(item, json.dumps(self.config[item])))
    
            command_type = self.config[item].get('command_type', None)
            if not command_type:
                raise KeyError("Command type not set")

            if command_type == "shell":
                task = ShellTask(item, self.config[item], self.logger)
            elif command_type == "mysql":
                task = MySQLTask(item, self.config[item], self.logger)
            else:
                raise ValueError("Unsupported command type")

            tasks.append(task)

            if self.config[item]['type'] == 'periodic':
                async_tasks.append(self.loop.create_task(periodic_task(task)))
            elif self.config[item]['type'] == 'continuous':
                async_tasks.append(self.loop.create_task(continuous_task(task)))

        self.logger.info(
            "{0} tasks found".format(len(tasks)))

        async_tasks.append(
            self.loop.create_task(
                flush_task(
                    loop=self.loop, logger=self.logger, tasks=tasks)))
        try:
            self.logger.info("Starting the main loop")
            self.loop.run_until_complete(asyncio.wait(async_tasks))
        except asyncio.CancelledError:
            pass
        self.loop.close()

    def signal_handler(self, signal, frame):
        self.logger.warning("Ctrl+C! Stopping tasks...")
        for task in asyncio.Task.all_tasks():
            task.cancel()

    def get_config(self, config_file):
        cnf = ConfigParser()
        try:
            cnf.read(config_file)[0]
        except IndexError:
            raise Exception("Configuration file could not be read")

        self.config = {}
        for section in cnf.sections():
            self.config[section] = {}
            for key, value in cnf.items(section):
                self.config[section][key] = value

        return self.config


class Task(object):
    def __init__(self, name, config, rootlogger=None):
        self.name = name
        self.config = config

        log_file = config.get('log_dir', None) + "/" + \
                   config.get('log_file', self.name)
        if not log_file:
            raise KeyError("Command log file not set")

        self.handler = CompressedTimedRotatingFileHandler(
            log_file,
            when=config.get('schedule', ROTATE_SCHEDULE),
            interval=1,
            backupCount=int(config.get('rotate', NUMBER_OF_FILES_TO_KEEP)),
            mode='at')

        self.rootlogger = rootlogger

        if hasattr(self, 'configure'):
            self.configure()

    def run(self):
        pass

    def flush(self):
        self.handler.flush()


class ShellTask(Task):
    def run(self):
        command = self.config.get('command', None)
        if not command:
            raise ValueError("Command not given")

        self.handler.emit("-- mark --")

        _command = shlex.split(command)
        proc = yield asyncio.From(
            asyncio.create_subprocess_exec(*_command, stdout=PIPE, stderr=PIPE))
        start = time.time()
        while True:
            if proc.stdout:
                line = yield asyncio.From(proc.stdout.readline())
                if sys.version_info[0] == 3:
                    self.handler.emit(line[:-1].decode('utf-8'))
                else:
                    self.handler.emit(line[:-1])
                if not line:
                    break
        cputime = time.time() - start
        exitcode = yield asyncio.From(proc.wait())


class MySQLTask(Task):
    def configure(self):
        try:
            config = {}

            if self.config.get('mysql_host', MYSQL_HOST):
                config['host'] = self.config.get('mysql_host', MYSQL_HOST)

            if self.config.get('mysql_socket', None):
                config['unix_socket'] = self.config.get('mysql_socket', None)

            if self.config.get('mysql_user', MYSQL_USER):
                config['user'] = self.config.get('mysql_user', MYSQL_USER)

            if self.config.get('mysql_passwd', MYSQL_PASSWD):
                config['pass'] = self.config.get('mysql_passwd', MYSQL_PASSWD)

            if self.config.get('mysql_default_file', MYSQL_DEFAULT_FILE):
                config['read_default_file'] = self.config.get('mysql_default_file', MYSQL_DEFAULT_FILE)
            
            self.connection = pymysql.connect(**config)
        except Exception as e:
            self.handler.emit("Cannot get connection to database: {0}".format(e))
            self.rootlogger.error("{1} : ERROR : Cannot get connection to database: {0}".format(e, self.name))

    def run(self):
        command = self.config.get('command', None)
        if not command:
            raise ValueError("Command not given")

        self.handler.emit("-- mark --")

        if not self.connection:
            try:
                self.configure()
            except:
                pass
            if not self.connection:
                self.handler.emit("No connection available.")
                self.rootlogger.error("{0} : ERROR : No connection to database".format(self.name))
                return

        try:
            cursor = self.connection.cursor()
            cursor.execute(command)
        except pymysql.err.OperationalError as e:
            self.connection = None
            self.handler.emit(str(e))
            return
        except Exception as e:
            self.handler.emit(str(e))
            return

        column_names = [i[0] for i in cursor.description]
        start = time.time()
        if cursor.rowcount > 0:
            while cursor.rowcount > cursor.rownumber:
                row = cursor.fetchone()
                if 'innodb status' in command or 'INNODB STATUS' in command:
                    for i in row[2].split('\n'):
                        self.handler.emit(i)
                else:
                    self.handler.emit(str(row))
        else:
            self.handler.emit("No output.")
        cputime = time.time() - start


@asyncio.coroutine
def continuous_task(task):
    current_retries = 0
    while current_retries < int(task.config.get('retries', CONTINOUS_RETRIES)):
        yield asyncio.From(task.run())
        current_retries += 1

@asyncio.coroutine
def periodic_task(task):
    while True:
        yield asyncio.From(task.run())
        yield asyncio.From(asyncio.sleep(int(task.config.get('interval', PERIODIC_INTERVAL ))))

@asyncio.coroutine
def flush_task(loop, logger, tasks):
    while True:
        for item in tasks:
            item.handler.flush()
        yield asyncio.From(asyncio.sleep(1))


def daemonize(options, stdin='/dev/null', stdout='/dev/null', 
              stderr='/dev/null'):
    '''This forks the current process into a daemon. The stdin, stdout, and
    stderr arguments are file names that will be opened and be used to replace
    the standard file descriptors in sys.stdin, sys.stdout, and sys.stderr.
    These arguments are optional and default to /dev/null. Note that stderr is
    opened unbuffered, so if it shares a file with stdout then interleaved
    output may not appear in the order that you expect. '''

    # Do first fork.

    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)  # Exit first parent.
    except OSError as e:
        sys.stderr.write("fork #1 failed: (%d) %s\n" % (e.errno, e.strerror))
        sys.exit(1)

    # Decouple from parent environment.
    # TODO: do we need to change to '/' or can we chdir to wherever __file__ is?
    os.chdir("/")
    os.umask(0)
    os.setsid()

    # Do second fork.
    try:
        pid = os.fork()
        if pid > 0:
            f = open(options.pidfile, "w")
            f.write(str(pid))
            f.close()
            sys.exit(0)  # Exit second parent.
    except OSError as e:
        sys.stderr.write("fork #2 failed: (%d) %s\n" % (e.errno, e.strerror))
        sys.exit(1)

    # Now I am a daemon!

    # Redirect standard file descriptors.
    si = open(stdin, 'r')
    so = open(stdout, 'a+')
    se = open(stderr, 'a+')
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())


def main():
    op = argparse.ArgumentParser()
    op.add_argument("-c", "--config", dest="config", required=False,
                    help="Configuration file", default='/etc/dbmon.conf')
    op.add_argument("-p", "--pidfile", dest="pidfile", 
                    default="/var/run/dbmon.pid",
                    help="PID file location")
    op.add_argument("-d", "--debug", dest="debug", default=False, 
                    action="store_true",
                    help="Log debug messages",
                    )
    op.add_argument("-f", "--foreground", dest="foreground", default=False,
                    action="store_true",
                    help="Don't fork into background"
                    )
    op.add_argument("-v", "--version", dest="version", default=False, action="store_true")

    options = op.parse_args()
    if not options:
        op.print_help()
        sys.exit(1)

    if options.version:
        print("dbmon version: {0}".format(__version__))
        sys.exit(0)

    if not os.path.exists(options.config) or not os.path.isfile(options.config):
        print("{0} is not a configuration file!".format(options.config))
        op.print_help()
        sys.exit(1)

    try:
        dbmon = DbMon(options)
    except Exception as e:
        print(e)
        sys.exit(1)

    if not options.foreground:
        daemonize(options=options, stdin='/dev/null', stdout=dbmon.log_file,
                  stderr=dbmon.log_file)
    try:
        dbmon.run()
    except Exception as e:
        dbmon.logger.info(e)
        print(e)
        sys.exit(1)
